MindSpore Transformer Document
==============================

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Installation Deployment

   mindformers_install

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: BERT Fine adjustment

   mindformers_bert_finetune

.. toctree::
   :maxdepth: 1
   :caption: API References

   mindformers
   mindformers.core
   mindformers.dataset
   mindformers.models
   mindformers.modules
   mindformers.pipeline
   mindformers.trainer
   mindformers.wrapper

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: RELEASE NOTES

   RELEASE