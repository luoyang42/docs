# Dataset Usages Cookbook

<a href="https://gitee.com/mindspore/docs/blob/master/tutorials/experts/source_en/dataset/dataset_more_usages.md" target="_blank"><img src="https://mindspore-website.obs.cn-north-4.myhuaweicloud.com/website-images/master/resource/_static/logo_source_en.png"></a>

MindSpore dataset provides variable data processing methods, including predefined data pipeline modules, customized data processing, data processing running configuration, data processing debugging and optimization, and multi-type data processing. It allows users to flexibly define the required data processing flow and process large-scale data efficiently.

This article introduces data processing code in various cases as a reference for users to do data processing. **(Continuously update...)**

## Prepare data and environment

Please run the following code block to prepare some data before running the example code we provide.

```python
from download import download

url1 = "https://obs.dualstack.cn-north-4.myhuaweicloud.com/mindspore-website/notebook/datasets/apple.jpg"
url2 = "https://obs.dualstack.cn-north-4.myhuaweicloud.com/mindspore-website/notebook/datasets/banana.jpg"
url3 = "https://obs.dualstack.cn-north-4.myhuaweicloud.com/mindspore-website/notebook/datasets/orange.jpg"

download(url1, './apple.jpg', replace=True)
download(url2, './banana.jpg', replace=True)
download(url3, './orange.jpg', replace=True)
```

## Connect custom data processing methods to the data pipeline

MindSpore dataset allows users to define data processing methods and insert them into the dataset pipeline, then the given methods will be run on the dataset pipeline in parallel.

In the following example, we define a UdfProcessor method that contains the logic to process the image using OpenCV, you can also add more data processing strategies as you need.

To insert the UdfProcessor method into the dataset pipeline, we only need to define a `map` operation node and pass the method to it. Then `map` node will help to apply the UdfProcessor's logic to each data sample.

Note: The `map` node can not only accept the function objects but also any callable objects are permitted.

```python
import cv2
import mindspore.dataset as ds

class ImageReader:
    def __init__(self):
        # use 3 images as source data
        self.image_path = ["./apple.jpg", "./banana.jpg", "./orange.jpg"]
    def __getitem__(self, idx):
        return self.image_path[idx]
    def __len__(self):
        return 3

def UdfProcessor(image_path):
    image = cv2.imread(image_path.item())
    image = cv2.resize(image, (30, 30))
    return image

# Define a source node to read data
train_data = ds.GeneratorDataset(ImageReader(), column_names="image")
# Define a map node to apply transform on each sample
# The UdfProcessor is a user-defined function to transform data,
# it receives the data column specified by `input_columns` and return after being transformed.
# Btw, the map node can not only accepts the function, but also accepts any callable object.
train_data = train_data.map(UdfProcessor, input_columns="image")

# Iter dataset and check data
for data in train_data:
    print(data)
```

## Data pipeline performance evaluation

In some scenarios, the user expects to obtain a data processing time to determine a performance bottleneck during network model training.

As we know, all dataset loading and enhancement operations are performed only on the CPU side. Therefore, you only need to find the defined dataset object before training the model, perform iteration operations on it, and work with the Python time library to obtain the time when each piece of data is processed and returned.

In the following example, we define a data pipeline and show the time when each piece of data is processed by the pipeline and returned to the output.

```python
import mindspore.dataset as ds
import time

# Assume that we have already prepared a dataset pipeline
# like train_data = ds.ImageFolderDataset(...) or train_data = ds.MindDataset(...).
# Here we take GeneratorDataset as example
train_data = ds.GeneratorDataset([i for i in range(100)], column_names="data")
train_data = train_data.map(lambda x: x+1, input_columns="data")
train_data = train_data.batch(20)

# After defining dataset, start a timer to check time of each batched data
avg_time = 0
start = time.time()
for i, data in enumerate(train_data):
    interval = time.time() - start
    avg_time = avg_time + interval
    print("step: {}, time: {}, avg: {}".format(i, interval, avg_time/(i+1)), flush=True)
    start = time.time()

# terminal result -> time cost(in second) of each batch
# step: 0, time: 0.0026092529296875, avg: 0.0026092529296875
# step: 1, time: 0.0001995563507080078, avg: 0.001404404640197754
# step: 2, time: 0.0003795623779296875, avg: 0.0010627905527750652
# step: 3, time: 0.0001456737518310547, avg: 0.0008335113525390625
# step: 4, time: 0.0003306865692138672, avg: 0.0007329463958740234
```

## Data pipeline preprocess balancing

MindSpore dataset support users to implement data processing logic on many operation nodes such as `GeneratorDataset`, `map` and etc. In some scenarios, a large amount of complex processing logic may be stacked on one data node, which results in slow performance of data processing.

Since each data processing node of the dataset pipeline is concurrent, the complex data processing logic can be flexibly adjusted to different data nodes, thereby reducing pressure on a single data processing operation node and balancing computing loads. In general, performance will be improved with this optimization.

The following shows an example code of stacking data processing logic on one data node and also how that to optimize it.

```python
import numpy as np
import mindspore.dataset as ds
import mindspore.dataset.vision as vision
import mindspore.dataset.transforms as transform

class DataLoader:
    def __init__(self):
        # use 3 images as source data
        self.image_path = ["./apple.jpg", "./banana.jpg", "./orange.jpg"]
        self.label = [0, 1, 2]
    def __getitem__(self, idx):
        # transform image
        img = np.fromfile(self.image_path[idx])
        img = vision.Decode()(img)
        img = vision.Resize((256, 256))(img)
        img = vision.RandomCrop((224, 224))(img)
        img = vision.HWC2CHW()(img)
        # transform label
        label = self.label[idx]
        label = label + 1
        return img, label
    def __len__(self):
        return 3

# Define a source node to read data
train_data = ds.GeneratorDataset(DataLoader(), column_names=["image", "label"])

# Iter dataset and check data
for data in train_data:
    print(data)
```

In the example code, train_dataset has only one data processing node `GeneratorDataset`, and all data processing operations are completed in `__getitem__`. There is no problem with writing data processing logic in this way, but if performance is considered, the data processing performance may not be optimal.

You can use the `map` node to split the data processing logic, for example, into the following forms:

```python
import numpy as np
import mindspore.dataset as ds
import mindspore.dataset.vision as vision
import mindspore.dataset.transforms as transform

class DataLoader:
    def __init__(self):
        # use 3 images as source data
        self.image_path = ["./apple.jpg", "./banana.jpg", "./orange.jpg"]
        self.label = [0, 1, 2]
    def __getitem__(self, idx):
        # load image only
        img = np.fromfile(self.image_path[idx])
        # load label only
        label = self.label[idx]

        return img, label
    def __len__(self):
        return 3

# Define a source node to read data
train_data = ds.GeneratorDataset(DataLoader(), column_names=["image", "label"])

# Apply image transform on this map node
# Note that vision.X are the internal transformations in MindSpore, thus we can pass these classes to map directly
train_data = train_data.map([vision.Decode(), vision.Resize((256, 256)), vision.RandomCrop((224, 224)), vision.HWC2CHW()], input_columns="image")

def label_transform(label):
    label = label + 1
    return label
# Apply image transform on this map node
# Note that label add is a user-defined behavior so we pass the python function to map node
train_data = train_data.map(label_transform, input_columns="label")

# Iter dataset and check data
for data in train_data:
    print(data)
```

## Dataset pipeline handle arbitrary Python data types

In MindSpore 2.0 and later versions, the dataset allows Python types to be freely transferred in the dataset pipeline and used in any part of the pipeline.

To achieve it, you only need to wrap the Python type to be transferred into the `Python dict` type, that is, `dict("key": Python obj)`.

In the following example, we define a class ReadImage that contains the decoder function, but instead of calling its decoding function in `__geiitem__`, we wrap it in `dict` and pass it to the next`map`operation node. In the`map`node, we take it out, decode the image stored in the "file" key in the dictionary, and return it. Finally, since the "decoder" function is usually not required for network training, we can also delete it from the dictionary.

```python
import mindspore.dataset as ds
import mindspore.dataset.vision as vision
import numpy as np

class ImageReader:
    def __init__(self):
        # use 3 banana images as source data
        self.image_path = ["./apple.jpg", "./banana.jpg", "./orange.jpg"]
        self.label = [0, 1, 2]
    def __getitem__(self, idx):
        data_pack = dict()
        data_pack["file"] = self.image_path[idx]
        data_pack["label"] = self.label[idx]
        data_pack["decoder"] = self.decoder
        return data_pack
    def __len__(self):
        return 3
    def decoder(self, img):
        img = np.fromfile(img)
        img = vision.Decode()(img)
        return img

# Define a source node to read data
train_data = ds.GeneratorDataset(ImageReader(), column_names="data")

# Define a map node to apply transform on each sample
def UdfProcessor(data_pack):
    file_path = data_pack["file"]
    decoder = data_pack["decoder"]
    image = decoder(file_path)
    # save back the decoded image to dict
    data_pack["file"] = image
    return data_pack
train_data = train_data.map(UdfProcessor, input_columns="data")

# Drop the "decoder" from dataset pipeline since we don't need it in train
def PopDict(data_pack):
    data_pack.pop("decoder")
    return data_pack
train_data = train_data.map(PopDict, input_columns="data")

# Iter dataset and check data
for data in train_data:
    print(data)
```

## Dataset pipeline outputs dict data to the network model for training

In MindSpore 2.0 and later versions, datasets can encapsulate data into dicts and transfer the dicts to constructs of network models. In the following, we introduce the code usage examples in non-sink data mode and sink data mode.

The first is the non-sink data mode. We can directly iterate the dataset to obtain the dict data, transfer the data to the network directly, and obtain the corresponding result based on the compute of the network construct.

```python
import mindspore
from mindspore import nn, ops
import mindspore.dataset as ds
import numpy as np

class Network(nn.Cell):
    def __init__(self):
        super().__init__()
        self.flatten = nn.Flatten()
        self.dense_relu_sequential = nn.SequentialCell(
            nn.Dense(28*28, 512),
            nn.ReLU(),
            nn.Dense(512, 512),
            nn.ReLU(),
            nn.Dense(512, 10)
        )

    def construct(self, x):
        # Get data with key "image"
        x = x["image"]
        x = self.flatten(x)
        logits = self.dense_relu_sequential(x)
        return logits

class DataLoder:
    def __init__(self):
        self.data = [np.ones((1, 28, 28), dtype=np.float32) for i in range(10)]
    def __getitem__(self, idx):
        return {"image": self.data[idx]}
    def __len__(self):
        return 10

model = Network()
dataset = ds.GeneratorDataset(DataLoder(), "data_col")
dataset = dataset.batch(5)

# Iter dataset and pass dict to network directly
for data in dataset:
    logits = model(data[0])
    print(logits)
```

In the data sink mode, the data sink channel is not fully supported for now. To use this feature, you need to change to another way.

The following example shows how data of type dict is currently passed to the network model in data sink mode.

ps: The data sink mode takes effect only in the GPU and Ascend environments.

```python
import mindspore as ms
from mindspore import nn, ops
import mindspore.dataset as ds
import numpy as np

class DataLoder:
    def __init__(self):
        self.data = [np.ones((1, 28, 28), dtype=np.float32) for i in range(10)]
        self.label = [np.array(0, dtype=np.int32) for i in range(10)]
    def __getitem__(self, idx):
        return {"image": self.data[idx], "label": self.label[idx]}
    def __len__(self):
        return 10

# In sink mode, we should convert dict to tuple before being passed to network
def Dict2Tuple(data_col):
    return data_col["image"], data_col["label"]

# Generate source data
ds_train = ds.GeneratorDataset(DataLoder(), "data_col")
# Convert dict to tuple. After Dict2Tuple, we get extra data columns, so we should specify output_columns
ds_train = ds_train.map(Dict2Tuple, input_columns="data_col", output_columns=["image", "label"])
# batch the data
ds_train = ds_train.batch(2)

# Define network
class Network(nn.Cell):
    def __init__(self):
        super().__init__()
        self.flatten = nn.Flatten()
        self.dense_relu_sequential = nn.SequentialCell(
            nn.Dense(28*28, 512),
            nn.ReLU(),
            nn.Dense(512, 512),
            nn.ReLU(),
            nn.Dense(512, 10)
        )

    def construct(self, x):
        x = x
        x = self.flatten(x)
        logits = self.dense_relu_sequential(x)
        return logits

def forward_fn(data, label):
    loss = net_loss(network(data), label)
    return loss

network = Network()
network.set_train()
weights = network.trainable_params()
net_loss = nn.SoftmaxCrossEntropyWithLogits(sparse=True, reduction="mean")
net_opt = nn.Momentum(network.trainable_params(), 0.01, 0.9)
grad_fn = ms.value_and_grad(forward_fn, None, weights)

def train_step(data, label):
    loss, grads = grad_fn(data, label)
    net_opt(grads)
    return loss

# Start train
epochs = 3
data_size = ds_train.get_dataset_size()
train_steps = epochs * data_size

# Enable dataset sink mode
sink_process = ms.data_sink(train_step, ds_train, jit_config=ms.JitConfig())

for _ in range(train_steps):
    loss = sink_process()
    print(f"epoch {_ + 1}, loss is {loss}")
```